<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'tomorrow_wp701' );

/** MySQL database username */
define( 'DB_USER', 'tomorrow_wp701' );

/** MySQL database password */
define( 'DB_PASSWORD', 'SQ2)3p8.6V' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'u6ijldvav7m7duixhzyblo4e4lz8xgiqfrky54sllrlo9xzj2vsmom7seohqiqgf' );
define( 'SECURE_AUTH_KEY',  '4osybcpqwpa0iaazkxyww08ckhtxzdgznwvfp39higmpcaii7mqhhvn3qxpohkl6' );
define( 'LOGGED_IN_KEY',    'dsm986bgoj75gdhonjii8heknno9bt3seobked0glkjvisno9hlyrr5w2ucbubp8' );
define( 'NONCE_KEY',        'wf41w1oigqvass8ln2zy54kt6a6ulsubgryw8xyi9sldkg85nzw7e770jckwjnob' );
define( 'AUTH_SALT',        'b9icwalbgqeaz555xqbmt5nluk7bjo5oimuoszluh0hak8zzfs83r7j8kg4jmttb' );
define( 'SECURE_AUTH_SALT', 'v3gyvquyymee1optas6nh8n4kuvtz6z8xtlttq9li01i2hhmzt8czwnwbogle8qi' );
define( 'LOGGED_IN_SALT',   '8ahrpb3itkt47ggicrjqffofxgxpdgohxszdj1eorfmplqjquvzabcrryk0bm7bv' );
define( 'NONCE_SALT',       '3uyy9xbdan2gubqlwvrkp0oqltm3jk1t8iudchbjinruxmexrbngfeshv8rt0bvr' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wpwl_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
