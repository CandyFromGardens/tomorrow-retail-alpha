<?php /* Template Name: Case Study1 page  */
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package tomorrow
 */

get_header();
?>

<div class="section left_banner_section scrollin_p">
    <div class="left_bg_photo parallax-window" data-parallax="scroll" data-image-src="<?php bloginfo('template_directory'); ?>/images/case1/case1.jpg"></div>
    
</div>

<div class="section full_section  case_content_section ">
    <div class="section_inwrapper s_section_inwrapper">
        <div class="col_wrapper ">
            <div class="row">
                <div class="col col12 align_center title_col blue_text" >
                    <div class="col_spacing  scrollin scrollin2">
                        <h1 class="bold">CASE STUDY : 01</h1>
                        <h2 class="allcap">DIGITIZING THE STORE: IMPROVING CONVENIEINCE WHILE CREATING A LONG-TERM CUSTOMER ENGAGEMENT PLATFORM (CHINA)</h2>
                    </div>
                </div>
                <div class="col col12">
                    <div class="col_spacing  scrollin scrollin2 ">
                        <h5 class="bold align_center allcap blue_text">Background</h5>
                        <p>
                        Global retailer with 400+ stores across China faced pressure from online and smaller-format stores.  Traffic was in a multi-year decline as customers found the retailer’s  stores inconvenient and unappealing.  Meanwhile, the retailer lacked a holistic digital membership program and therefore had limited ability to assess the reasons for customer churn or to re-engage lapsed customers.  
                        </p>
                    </div>
                </div>

                <div class="col col6">
                    <div class="col_spacing   scrollin scrollin2">
                        <div class="thumb_wrapper scrollin scrollin2">
                            <img src="<?php bloginfo('template_directory'); ?>/images/case1/case6.jpg" class="thumb "/>
                        </div>
                    </div>
                </div>

                <div class="col col6">
                    <div class="col_spacing   scrollin scrollin2">
                        <div class="thumb_wrapper scrollin scrollin2">
                            <img src="<?php bloginfo('template_directory'); ?>/images/case1/case5.jpg" class="thumb"/>
                        </div>
                    </div>
                </div>

                <div class="col col12">
                    <div class="col_spacing   scrollin scrollin2">
                        <h5 class="bold align_center allcap blue_text">Approach</h5>
                        <p>
                        Tomorrow understood the retailer’s customer needs and pain points.  Shop-alongs, focus groups, and in depth customer interviews all helped to identify the friction points causing customers to reduce trips.  Customers struggled to find merchandise in stores, identify relevant promotions and, most importantly, check out.  Customer viewed wait times at the retailer’s stores as being a key reason for going elswhere.
                        </p>

                        <p>
                        Tomorrow outlined a path to creating a Digital Platform which customers could use to alleviate these pain points. The platform would help customers find the products they need; provide personalized recommendations on promotions; most importantly, help them avoid the checkout lines by allowing them to scan and pay.  Tomorrow launched version one of the platform in ten weeks with an agile development approach -  beginning with a proof of concept in one store.
                        </p>
                    </div>
                </div>

                <div class="col col12">
                    <div class="col_spacing   scrollin scrollin2">
                        <div class="thumb_wrapper scrollin scrollin2">
                            <img src="<?php bloginfo('template_directory'); ?>/images/case1/online grocery2.jpg" class="thumb"/>
                        </div>
                    </div>
                </div>

                <div class="col col12">
                    <div class="col_spacing   scrollin scrollin2">
                        <h5 class="bold align_center allcap blue_text">Results</h5>
                        <p>
                        Customers immediately saw the benefit of the Digital Platform. Tomorrow led the initiative from POC to scale with over 400 stores activated.  Customer adoption moved quickly from 7% in POC to nearly 50% in the best stores, all within 12 months. Further, with the data collected from user's on the platform, Tomorrow led the client to develop a machine-learning-based personalization engine to provide personalized item recommendations and promotions. This has quickly emerged as a key competitive advantage for the client over its peers, and has helped the client attract additional brand investment from suppliers who are seeking this type of efficient, 1-to-1 channel to grow their sales.
                        </p>
                    </div>
                </div>
                <div class="clear"></div>
            </div>    
        </div>
    </div>
</div>

<?php
get_footer();
