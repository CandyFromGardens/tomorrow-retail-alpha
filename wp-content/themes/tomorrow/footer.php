<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package tomorrow
 */

?>

<div class="section footer_section scrollin_p">
    <div class="section_inwrapper">
        <div class="table">
            <div class="td text6">© Copyright 2020 Tomorrow</div>
        </div>
    </div>
</div>

<!-- loading -->
<div class="loading"><img src="<?php bloginfo('template_directory'); ?>/images/oval.svg"></div>


<div class="mobile_show"></div>

<div class="mobile_hide"></div>

<?php wp_footer(); ?>

</body>
</html>
