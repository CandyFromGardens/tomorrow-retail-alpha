<?php /* Template Name: Case Study1 page  */
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package tomorrow
 */

get_header();
?>

<div class="section left_banner_section scrollin_p">
    <div class="left_bg_photo parallax-window" data-parallax="scroll" data-image-src="<?php bloginfo('template_directory'); ?>/images/case1/case1.jpg"></div>
    
</div>

<div class="section full_section  case_content_section ">
    <div class="section_inwrapper s_section_inwrapper">
        <div class="col_wrapper ">
            <div class="row">
                <div class="col col12 align_center title_col blue_text" >
                    <div class="col_spacing  scrollin scrollin2">
                        <h1 class="bold">CASE STUDY : 01</h1>
                        <h2 class="allcap">DIGITIZING THE STORE:<br>
                        IMPROVING CONVENIENCE AND LONG-TERM CUSTOMER ENGAGEMENT</h2>
                    </div>
                </div>
                <div class="col col12">
                    <div class="col_spacing  scrollin scrollin2 ">
                        <h5 class="bold align_center allcap blue_text">Background</h5>
                        <p>
                        A global retailer with 400+ stores across China faced pressure from online and smaller-format stores. Traffic was in a multi-year decline because customers found the retailer’s stores inconvenient and unappealing. Furthermore, the retailer lacked a holistic digital membership program and therefore had limited ability to assess the reasons for customer churn or to re-engage lapsed customers.
                        </p>
                    </div>
                </div>

                <div class="col col6">
                    <div class="col_spacing   scrollin scrollin2">
                        <div class="thumb_wrapper scrollin scrollin2">
                            <img src="<?php bloginfo('template_directory'); ?>/images/case1/case6.jpg" class="thumb "/>
                        </div>
                    </div>
                </div>

                <div class="col col6">
                    <div class="col_spacing   scrollin scrollin2">
                        <div class="thumb_wrapper scrollin scrollin2">
                            <img src="<?php bloginfo('template_directory'); ?>/images/case1/case5.jpg" class="thumb"/>
                        </div>
                    </div>
                </div>

                <div class="col col12">
                    <div class="col_spacing   scrollin scrollin2">
                        <h5 class="bold align_center allcap blue_text">Approach</h5>
                        <p>
                        Tomorrow conducted shop-alongs, focus groups, and in-depth customer interviews to identify the friction points causing customers to reduce trips. Our research showed that customers struggled to find merchandise, to identify relevant promotions and -- most important – to check out quickly.  Customers pinpointed the wait times as a key reason to shop elsewhere.
                        </p>

                        <p>
                        To alleviate these pain points, Tomorrow proposed the creation of digital platform.  The platform would help customers find products, provide personalized recommendations on promotions, and streamline checkout through a scan-and-pay system.
                        </p>

                        <p>
                        By using an agile development approach, Tomorrow launched the platform within 10 weeks.  The rollout began with a proof of concept in one store and subsequently expanded to over 400 stores.  While Tomorrow was leading this initiative, we were also analyzing data collected from users of the platform.  Our analysis informed the development of another platform feature: an engine based on machine learning that enhances the personalization of item recommendations and promotions.
                        </p>
                    </div>
                </div>

                <div class="col col12">
                    <div class="col_spacing   scrollin scrollin2">
                        <div class="thumb_wrapper scrollin scrollin2">
                            <img src="<?php bloginfo('template_directory'); ?>/images/case1/online grocery2.jpg" class="thumb"/>
                        </div>
                    </div>
                </div>

                <div class="col col12">
                    <div class="col_spacing   scrollin scrollin2">
                        <h5 class="bold align_center allcap blue_text">Results</h5>
                        <p>
                        Customers immediately saw the benefit of the digital platform, and adoption moved quickly from 7% in POC to nearly 50% in the best stores, all within 12 months.
                        </p>

                        <p>
                        Meanwhile, the personalization engine has rapidly emerged as a key competitive advantage for our client, and it has attracted additional brand investment from suppliers who are seeking this type of efficient, 1-to-1 channel to grow their sales.
                        </p>
                    </div>
                </div>
                <div class="clear"></div>
            </div>    
        </div>
    </div>
</div>

<?php
get_footer();
