<?php /* Template Name: Home page  */
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package tomorrow
 */

get_header();
?>



<div class="section top_banner_section ">
    <div class="top_banner_slider_container swiper-container">
        <div class="top_banner_slider swiper-wrapper">
            <div class="top_banner_slider_item swiper-slide scrollin_p">
                <div class="bg_photo parallax-window" style="background-image:url(<?php bloginfo('template_directory'); ?>/images/photo/banner1.jpg)"></div>
                <div class="bg1"></div>
                <div class="float_text_wrapper">
                    <div class="spacing">
                        <div class="text_wrapper">
                            <div class="title text1 scrollin scrollin2">Tomorrow.<br>Digital Retail Delivered.</div>
                            <div class="description scrollin scrollin2">Tomorrow is a retail consulting firm focused on enabling leading retailers to accelerate their digital flywheel.</div>
                        </div>
                        <div class="btn_wrapper scrollin scrollin2">
                            <a href="http://tomorrowretail.com/case-study/case-study-01/" class="border_btn border_btn_r_arrow text7">Case Study</a>
                        </div>
                    </div>
                </div>
                <div class="dot_line dot_line1  scrollin scrollin_left"><div class="dot_bg_wrapper"><div class="dot_bg scrollin scrollin_right" style="background-image: url(<?php bloginfo('template_directory'); ?>/images/dot_bg1.png); background-size: 7px 8px;" ></div></div></div>
                <div class="dot_line dot_line2  scrollin scrollin_right"><div class="dot_bg_wrapper"><div class="dot_bg scrollin scrollin_left" style="background-image: url(<?php bloginfo('template_directory'); ?>/images/dot_bg1.png); background-size: 7px 8px;" ></div></div></div>
            </div>

           
        </div>
    </div>


    
</div>

<div class="section center_slider_section scrollin_p" data-id="Services">
    <div class="section_inwrapper">
        <div class="section_title text1  scrollin scrollin2">Services</div>
        <div class="center_slider_wrapper  scrollin scrollin2">
            <div class="center_slider_container swiper-container">
                <div class="center_slider swiper-wrapper">
                    <div class="center_slider_item swiper-slide">
                        <div class="text_wrapper">
                            <div class="spacing">
                                <div class="num">1</div>
                                <div class="title text0">Digital Channel Strategy</div>
                                <div class="description">Drive growth through native apps, social channels, and B2C platforms – with clear objectives and approaches for each.  We’ve built successful digital strategies for the world’s largest retailers – let us help you win in every channel.</div>
                            </div>
                        </div>
                    </div>
                    <div class="center_slider_item swiper-slide">
                        <div class="text_wrapper">
                            <div class="num">2</div>
                            <div class="title text0">Store-Based Fulfillment</div>
                            <div class="description">Physical stores become world-class fulfillment centers with the right approach.  We design and implement online fulfillment at scale through strategy, technology and smart operating models.  </div>
                        </div>
                    </div>
                    <div class="center_slider_item swiper-slide">
                        <div class="text_wrapper">
                            <div class="num">3</div>
                            <div class="title text0">In Store Digitization</div>
                            <div class="description">Use digital product design to remove friction from your store experience, while improving customer intelligence.  We’ve developed some of the world’s most successful digital products for in store use: let’s digitize your experience together.</div>
                        </div>
                    </div>
                    <div class="center_slider_item swiper-slide">
                        <div class="text_wrapper">
                            <div class="num">4</div>
                            <div class="title text0">Customer Data Strategy</div>
                            <div class="description">Create truly defendable competitive advantage through customer data.  We will help you design a data foundation and begin your machine-learning deployment to create a personalized future for your customer.   </div>
                        </div>
                    </div>
                    <div class="center_slider_item swiper-slide">
                        <div class="text_wrapper">
                            <div class="num">5</div>
                            <div class="title text0">Organizational Design for Digital</div>
                            <div class="description">Maintain your core while creating a digital flywheel to accelerate growth.  Start with the right org design and incentives.  </div>
                        </div>
                    </div>
                    <div class="center_slider_item swiper-slide">
                        <div class="text_wrapper">
                            <div class="num">6</div>
                            <div class="title text0">Concept Incubation and Launch</div>
                            <div class="description">Identify your future growth engines and create the space for success.  We are ready to help you identify, build and launch your next growth initiative.       </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="center_slider_nav  ">
                <a class="center_slider_prev scrollin scrollin2"></a>
                <a class="center_slider_next scrollin scrollin2"></a>
            </div>
        </div>
        <!--
        <div class="media_btn_wrapper   scrollin scrollin2">
            <a href="<?php echo esc_url( get_page_link( '18') ); ?>" class="media_btn">
                <div class="text">
                    <div class="text3 title">Media</div>
                    <div class="text6 description">More articles and videos</div>
                </div>
            </a>
        </div>
        -->
    </div>
</div>


<div class="section full_section circle_col_section blue_section scrollin_p" data-id="Key">
    <div class="section_inwrapper">
        <div class="section_title text1  scrollin scrollin2">Building the Digital Flywheel:<br/>Four Key Enablers</div>
        <div class="col_wrapper xl_col_wrapper">
            <div class="row">
                <div class="col col3">
                    <div class="col_spacing  scrollin scrollin2">
                        <div class="num">1</div>
                        <div class="thumb_wrapper">
                            <div class="circle_thumb" style="background-image: url(<?php bloginfo('template_directory'); ?>/images/photo/5.jpg);"></div>
                        </div>
                        <div class="text_wrapper ">
                            <div class="t1">Turn stores</div>
                            <div class="t2">into Distribution Hubs</div>
                        </div>
                    </div>
                </div>
                <div class="col col3">
                    <div class="col_spacing  scrollin scrollin2">
                        <div class="num">2</div>
                        <div class="thumb_wrapper">
                            <div class="circle_thumb" style="background-image: url(<?php bloginfo('template_directory'); ?>/images/photo/2.jpg);"></div>
                        </div>
                        <div class="text_wrapper ">
                            <div class="t1">Develop</div>
                            <div class="t2">Digital Sales Channels</div>
                        </div>
                    </div>
                </div>
                <div class="col col3">
                    <div class="col_spacing  scrollin scrollin2">
                        <div class="num">3</div>
                        <div class="thumb_wrapper">
                            <div class="circle_thumb" style="background-image: url(<?php bloginfo('template_directory'); ?>/images/photo/3.jpg);"></div>
                        </div>
                        <div class="text_wrapper ">
                            <div class="t2">Digitize</div>
                            <div class="t1">Customer Engagement</div>
                        </div>
                    </div>
                </div>
                <div class="col col3">
                    <div class="col_spacing  scrollin scrollin2">
                        <div class="num">4</div>
                        <div class="thumb_wrapper">
                            <div class="circle_thumb" style="background-image: url(<?php bloginfo('template_directory'); ?>/images/photo/4.jpg);"></div>
                        </div>
                        <div class="text_wrapper ">
                            <div class="t1">Personalize</div>
                            <div class="t2">Across Channels</div>
                        </div>
                    </div>
                </div>
                <div class="clear"></div>
            </div>    
        </div>
    </div>
</div>

<div class="section full_section logo_section green_section scrollin_p" data-id="Clients">
    <div class="section_inwrapper">
        <div class="section_title text1  scrollin scrollin2">Client Success.</div>
        <div class="section_description  scrollin scrollin2 col4">We work with world’s leading retailers and digital platforms to create digital retail success. </div>
        <div class="col_wrapper xl_col_wrapper">
            <div class="row">
                <div class="col col3">
                    <div class="col_spacing  scrollin scrollin2">
                        <div class="logo_wrapper">
                            <div class="h_logo"><img src="<?php bloginfo('template_directory'); ?>/images/logo_walmart.png" ></div>
                        </div>
                        <div class="text_wrapper text6">
                            Accelerating the world’s leading Digital Retailer  
                        </div>
                    </div>
                </div>
                <div class="col col3">
                    <div class="col_spacing scrollin scrollin2">
                        <div class="logo_wrapper">
                            <div class="h_logo"><img src="<?php bloginfo('template_directory'); ?>/images/logo_tencent.png" ></div>
                        </div>
                        <div class="text_wrapper text6">
                            Developing Smart Retail innovations
                        </div>
                    </div>
                </div>
                <div class="col col3 m_col">
                    <div class="col_spacing  scrollin scrollin2">
                        <div class="logo_wrapper">
                            <div class="h_logo"><img src="<?php bloginfo('template_directory'); ?>/images/logo_sam.png" ></div>
                        </div>
                        <div class="text_wrapper text6">
                            Building new channels for growth
                        </div>
                    </div>
                </div>
                <div class="col col3">
                    <div class="col_spacing scrollin scrollin2">
                        <div class="logo_wrapper">
                            <div class="h_logo"><img src="<?php bloginfo('template_directory'); ?>/images/logo_jardines.png" ></div>
                        </div>
                        <div class="text_wrapper text6">
                            Re-inventing Asia retail 
                        </div>
                    </div>
                </div>
                <div class="clear"></div>
            </div>    
        </div>
    </div>
</div>



<div class="section center_slider_section center_slider_section2 scrollin_p" data-id="Testimonials">
    <div class="section_inwrapper">
        <div class="section_title text1  scrollin scrollin2">Testimonials</div>
        <div class="center_slider_wrapper  scrollin scrollin2">
            <div class="center_slider_container swiper-container">
                <div class="center_slider swiper-wrapper">

                    <div class="center_slider_item swiper-slide">
                        <div class="text_wrapper">
                            <div class="spacing">
                                <div class="description quote text2">Tomorrow has been an outstanding consulting partner to Walmart’s Store No. 8, bringing a unique combination of innovation and operational knowledge.  Their insight into omnichannel innovation in China has been exceptionally helpful along with their ability to deliver creative ideas with the potential to scale.</div>
                                <div class="text5 author">Marc Lore<br>CEO Walmart eCommerce</div>
                            </div>
                        </div>
                    </div>

                    <div class="center_slider_item swiper-slide">
                        <div class="text_wrapper">
                            <div class="spacing">
                                <div class="description quote text2">Tomorrow demonstrated extensive expertise in both online and offline retail in our collaboration. It successfully consolidates the best practices in the US and China, the largest two consumer markets, to shed light on how retail leaders should think about digitizing the business.</div>
                                <div class="text5 author">Mark Wang<br>Director, Tencent Smart Retail</div>
                            </div>
                        </div>
                    </div>

                    <div class="center_slider_item swiper-slide">
                        <div class="text_wrapper">
                            <div class="spacing">
                                <div class="description quote text2">Tomorrow’s Jordan Berke has provided valuable analysis and advice to Sam’s Club as we pursue new growth opportunities.  He has a strong understanding of retail and consistently provides strategic recommendations that are pragmatic, actionable, and delivered in a highly collaborative way. </div>
                                <div class="text5 author">Lori Flees<br>Senior Vice President, Health & Wellness, Sam’s Club</div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <div class="center_slider_nav  ">
            <a class="center_slider_prev scrollin scrollin2"></a>
            <a class="center_slider_next scrollin scrollin2"></a>
        </div>
    </div>
</div>


<div class="section tab_section blue_section scrollin_p" data-id="AboutUs">
    <div class="hidden_tab active" data-id="about">
    <div class="tab_btn_wrapper">
        <a class="tab_btn col col4 text1 scrollin scrollin2" href="#aboutUs">
            <div class="table_wrapper_wrapper">
                <div class="table_wrapper"><div class="table"><div class="td">About Us</div></div></div>
            </div>
        </a>
        <a class="tab_btn col col4 text1 scrollin scrollin2" href="#team1">
            <div class="table_wrapper_wrapper">
                <div class="table_wrapper"><div class="table"><div class="td">Team</div></div></div>
            </div>
        </a>
        <a class="tab_btn col col4 text1 scrollin scrollin2" href="<?php echo esc_url( get_page_link( '18') ); ?>">
            <div class="table_wrapper_wrapper">
                <div class="table_wrapper"><div class="table"><div class="td">Media</div></div></div>
            </div>
        </a>
        
        <div class="clear"></div>
    </div>
    </div>

    <div class="hidden_tab" data-id="aboutUs">
        <div class="section_inwrapper">
            <div class="title_wrapper">
                <div class="title text1">About Us</div>
                <a class="side_btn border_btn border_btn_r_arrow_back text5" href="#about">back</a>
            </div>
            <div class="big_col_wrapper col_wrapper text7">
                <div class="row">
                    <div class="col col6">
                        <div class="col_spacing">
                            <p>Tomorrow is a global strategic retail consulting firm that advises leading retailers, brands, and digital platforms on how to thrive in today’s digitally-driven retail environment.  The firm was founded with the mission to help clients to <strong>combine the best of physical retail with the power of digital innovation, machine learning and personalization</strong>.</p>
 
                            <p>Tomorrow understands the challenges that arise in working through the digital transformation.  We have lived the journey and can provide trusted advice on how to successfully transform retail teams and business functions to operate as digital-first.   We live retail and embrace the people and processes that make retail such a unique industry.  We know that re-inventing a retail business takes a thoughtful, practical approach; we also understand retail is a results-driven industry and share our clients’ passion for delivering results along every step of the journey.</p>
                            
                            <p>We advise some of the world’s largest retailers on how to transform their businesses to be successful in today’s digital-retail marketplace.  We also guide digital platforms to develop strategies for how to empower retailers to effectively leverage digital innovation to differentiate themselves.</p>
                            
                            <p>Our foundation is retail.  Our backgrounds are in building and leading retail businesses.  Our passion is in helping clients who lead today to win tomorrow.</p>
                        </div>
                    </div>
                    <div class="col col6">
                        <div class="col_spacing">
                            <h4>History of Tomorrow</h4>
    
                            <p>Tomorrow was born out of twenty years of retail and ecommerce experience, both in China and the United States.  This experience included leading retail organizations large and small through various digital transformation initiatives.  Tomorrow’s founder spent 15 years helping to transform Walmart, the world’s largest retailer, into being a truly digital-first organization.  He led Walmart’s eCommerce and Digital Innovation efforts in China for 10 years, during which he developed and led the firm’s strategic alliances and investments with Tencent, JD and many other local tech and ecommerce innovators.  Previous to Walmart, Jordan helped to lead Beyond Interactive, one of the world’s first full-service digital marketing firms, advising brands such as Reebok, General Motors, and P&G on how to digitize their marketing and sales efforts. </p>
                        </div>
                    </div>
                    <div class="clear"></div>
                </div>
            </div>
        </div>
    </div>

    <div class="hidden_tab" data-id="team1">
        <div class="section_inwrapper">
            <div class="title_wrapper">
                <div class="title">
                    <span class="text1">Team</span>
                    <ul class="hidden_tab_menu">
                        <li><a href="#team1" class="active">Founder, CEO</a></li><li><a href="#team2">Operations Director</a></li>
                    </ul>
                </div>
                <a class="side_btn border_btn border_btn_r_arrow_back text5" href="#about">back</a>
            </div>

            <div class="big_col_wrapper col_wrapper text7">
                <div class="row">
                    <div class="col col6">
                        <div class="col_spacing">
                            <h4>Jordan Berke</h4>
                        </div>
                    </div>
                    <div class="clear"></div>
                    <div class="col col6">
                        <div class="col_spacing">

                            <p>Jordan is one of the world’s leading omnichannel retail strategists and innovators.</p>
                            
                            <p>With over 20 years of digital, retail and marketing experience, Jordan was an early leader in online marketing in the United States before moving to lead Walmart’s e-commerce and in-store digital efforts in China, where he spent the past 10 years.</p>

                            <p>Beyond Interactive, one of the world’s first digital marketing agencies (which WPP bought in 2003) is where Jordan began working with leading brands to create digital acquisition and retention programs. Beyond established metrics that have become industry standards for digital-display media, CRM campaigns, and ad-tracking. The pioneering work the firm did has become such a fixed part of marketing that WPP phased out the Beyond Interactive name in 2009 as it incorporated the unit’s function in all its media operations.</p>

                            <p>In 2005, Jordan joined the world’s largest retailer, Walmart, holding positions in merchandising, operations, finance, strategy, and ecommerce. In 2010, he spearheaded the launch of e-commerce in China for Walmart’s wholesale club, Sam’s. Because mobile commerce is the anchor of China’s online sales, Berke drove an expansion of O2O offerings across multiple regions, gaining deep insight into both the dynamics of retail in China and the technical side of e- and m-commerce.</p>
                        </div>
                    </div>
                    <div class="col col6">
                        <div class="col_spacing">
                            <p>That experience led to the launch of Walmart’s redesigned online strategy in China in 2015, which Jordan’s team again led. He was instrumental in forging the company’s alliances with Tencent, JD, and New Dada. He led the expansion of Walmart's ecommerce business to include multiple offerings, including its one-hour fresh and grocery service, which now serves millions of customers across 30 cities.</p>

                            <p>Walmart's use of digital innovation and CRM in China helped the company make significant inroads to digitizing and personalizing its in-store experiences, and Berke was a driving force behind that change. Walmart's Scan & Go, a mini program within China’s top social app, WeChat, is another innovation that Jordan led and is now considered a prime example of retail digitalization worldwide. Berke was recognized with Walmart’s top executive honor, the Sam Walton Entrepreneur of the Year Award, in China in 2018 for his innovations around ecommerce; he was also named Chairman of the company’s Global Omnichannel Forum, guiding the company’s priorities around O2O and digital innovation.</p>

                            <p>At Tomorrow, Jordan is continuing his push to transform how customers experience the physical store, while providing brands with a platform to engage users on a truly 1-to-1 level</p>
                        </div>
                    </div>
                    <div class="clear"></div>
                </div>
            </div>
        </div>
    </div>

    <div class="hidden_tab" data-id="team2">
        <div class="section_inwrapper">
            <div class="title_wrapper">
                <div class="title">
                    <span class="text1">Team</span>
                    <ul class="hidden_tab_menu">
                        <li><a href="#team1">Founder, CEO</a></li><li><a href="#team2" class="active">Operations Director</a></li>
                    </ul>
                </div>
                <a class="side_btn border_btn border_btn_r_arrow_back text5" href="#about">back</a>
            </div>

            <div class="big_col_wrapper col_wrapper text7">
                <div class="row">
                    <div class="col col6">
                        <div class="col_spacing">
                            <h4>Alice Berke</h4>

                            <p>Alice was a digital marketing pioneer in Hong Kong and China before going on to found multiple sustainability-focused businesses in North America and Asia.</p>
                            
                            <p>Alice spent ten years with Grey Worldwide, a leading advertising group.  As the General Manager of Beyond Interactive Hong Kong, Grey’s digital advertising division, Alice built the market’s leading digital media agency, serving clients in all industries.</p> 
                            
                            <p>In 2007, Alice co-founded Nude Food, a Raw Food retail and catering business in Canada.  Her passion to provide healthy, sustainable food helped to create one of the early raw food success stories in Canada.</p>
                            
                            <p>In 2010, Alice joined well known Hong Kong entrepreneur Pui Feng ____ to build Fair Taste, a fair-trade focused brand for the Hong Kong market.  Fair Taste played an instrumental role in promoting fair trade products throughout Hong Kong.</p>
                            
                            <p>At Tomorrow, Alice leads Operations, developing processes and tools to ensure clients receive the highest-quality service.</p>   
                        </div>
                    </div>
                    <div class="clear"></div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="section full_section form_section scrollin_p" data-id="FollowUs">
    <div class="section_inwrapper">
        <div class="section_title text1 scrollin scrollin2">Follow us!<br/>get updates of tomorrow.</div>
        <div class="col_wrapper">
            <div class="row">
                <div class="col col6 text4">
                    <div class="col_spacing scrollin scrollin2">
                        <p>
                        1820 Avenue M, Suite 589<br>Brooklyn, NY 11230
                        </p>
                        <p>
                        E <a href="mailto:jordan@tomorrowretail.com">jordan@tomorrowretail.com</a><br>
                        T <a href="tel:+929-326-5357">+929 326 5357</a>
                        </p>
                    </div>
                </div>
                <div class="col col6">
                    <div class="col_spacing scrollin scrollin2">
                        <?php the_field("form"); ?>
                    </div>
                </div>
                <div class="clear"></div>
            </div>
        </div>
    </div>            
</div>


<?php
get_footer();
