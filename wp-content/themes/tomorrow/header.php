<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package tomorrow
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="https://gmpg.org/xfn/11">

	<?php wp_head(); ?>

    <link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png">
    <link rel="manifest" href="/site.webmanifest">
    <link rel="mask-icon" href="/safari-pinned-tab.svg" color="#25408f">
    <meta name="msapplication-TileColor" content="#25408f">
    <meta name="theme-color" content="#ffffff">
    <meta content="Tomorrow Retail: Leading Digital Retail Advisory" property="og:description" name="description">

</head>

<?php
$bodyclass="";
if(is_singular("case-study")){
$bodyclass="case_body";
}
?>

<body <?php body_class($bodyclass); ?>>
<?php wp_body_open(); ?>


<div class="header_bg mobile_show"></div>
<div class="header  scrollin_p">
    <div class="bg"></div>
    <a href="<?php echo esc_url( get_page_link( '8') ); ?>" class="top_logo scrollin scrollin2"><img src="<?php bloginfo('template_directory'); ?>/images/top_logo.png"></a>

    <a href="#" class="menu_btn scrollin scrollin2">
        <div class="line line1"></div>
        <div class="line line2"></div>
        <div class="line line3"></div>
    </a>

    <a href="#form" class="email_btn  scrollin scrollin2"></a>
</div>


<div class="dropdown_menu ">
        <ul>
            <li><a href="<?php echo esc_url( get_page_link( '8') ); ?>#Services">Services</a></li>
            <li><a href="<?php echo esc_url( get_page_link( '18') ); ?>">Media</a></li>
            <li><a href="<?php echo esc_url( get_page_link( '8') ); ?>#Key">Building the Digital Flywheel</a></li>
            <li><a href="<?php echo esc_url( get_page_link( '8') ); ?>#Clients">Clients</a></li>
            <li><a href="<?php echo esc_url( get_page_link( '8') ); ?>#Testimonials">Testimonials</a></li>
            <li><a href="<?php echo esc_url( get_page_link( '8') ); ?>#AboutUs">About Us</a></li>
            <li><a href="<?php echo esc_url( get_page_link( '8') ); ?>#FollowUs">Follow us</a></li>
            <li class="">
                <ul>
                    <li><a href="<?php echo esc_url( get_page_link( '29') ); ?>">Case Study 01</a></li>
                </ul>

            </li>
        </ul>
</div>
