

$(document).ready(function(){
	$( "a.email_btn" ).click(function( event ) {
        event.preventDefault();
        $("html, body").animate({ scrollTop: $($(this).attr("href")).offset().top }, 1500);
    });
});

if (!("ontouchstart" in document.documentElement)) {
document.documentElement.className += " no-touch";
}

var waitForFinalEvent = (function () {
  var timers = {};
  return function (callback, ms, uniqueId) {
    if (!uniqueId) {
      uniqueId = "Don't call this twice without a uniqueId";
    }
    if (timers[uniqueId]) {
      clearTimeout (timers[uniqueId]);
    }
    timers[uniqueId] = setTimeout(callback, ms);
  };
})();

	



function doscroll(){
	
	var scrolltop = $(window).scrollTop();
	var hh = $(window).height();
	
	if(scrolltop>100){
		$("body").addClass("nottop")
		$("body").addClass("nottop")
	}else{
		$("body").removeClass("nottop")
		$("body").removeClass("nottop")
		}
	
	$(".doscroll").each(function(){
		var $this = $(this);
		var mytop = $this.offset().top;
		var myh = $this.height();
		
		
		var dis = (scrolltop+hh)-mytop;
		
		
		if(dis>0 && dis<hh+myh){
			
			
			$this.addClass("onscreen");
		
			//console.log( Math.round((scrolltop/(mytop+myh))*100)/100);
			$this.find(".offset").each(function(){
				var childheight = parseInt($(this).css("height"));
				var range = Math.round($(this).attr("data-offset")*(hh/2)*10000)/10000;
				var factor = Math.round(((mytop-scrolltop+myh)/(hh+myh)-0.5)*10000)/10000;
				var ratio = Math.round((childheight/hh)*10000)/10000;
				
				if($(".mobile_show").is(":visible")){
				//range = range/2;
				}
				
				var scalefactor = 1
				
				//$(this).attr("data-range",range)
				//$(this).attr("data-height",range/2+myh)
				//$(this).attr("data-childheight",childheight)
				
				if($(this).hasClass("center_image")){
					if(range/2+myh>childheight){
						//$(this).attr("data-problem","yes")
						scalefactor=Math.round(((range/2+myh)/childheight)*100)/100;
					}else{
						//$(this).attr("data-problem","no")
						
					}
				}
				
				$(this).css({
					"transform": "translateY(" + parseInt(-1*range*factor) + "px) scale("+scalefactor+")",
				})
				
				
				$(this).parent().css({
					"opacity": 1
				})
				
				
			});
			
		}else{
			
			$this.removeClass("onscreen");
		}
		
		
	});
	
	
	if(scrolltop>hh/2){
		$(".fix_right").addClass("mhide");
	}else{
		$(".fix_right").removeClass("mhide");
	}
	
	$(".scrollin").not($(".scrollin_p .scrollin")).each(function(i){
		var $this = $(this);
		var mytop = $this.offset().top;
		var myh = $this.height();
		
		var dis = (scrolltop+hh)-mytop;
		
		if(dis>0 ){
			$this.removeClass("leavescreen");
			$this.addClass("onscreen");
			/*
			if(dis<hh+myh){
				$this.find(".scrollin").removeClass("stop");
			}else{
				$this.find(".scrollin").addClass("stop");
			}
			*/
		}else{
			$this.removeClass("onscreen");
			$this.addClass("leavescreen");
		}
	});
	
	
	$(".scrollin_p").each(function(i){
		var $this = $(this);
		var mytop = $this.offset().top;
		var myh = $this.height();
		
		var dis = (scrolltop+hh)-mytop;
		
		if(dis>0){
			
			$this.find(".scrollin").removeClass("leavescreen");
			$this.find(".scrollin").addClass("onscreen");
			/*
			if(dis<hh+myh){
				$this.find(".scrollin").removeClass("stop");
			}else{
				$this.find(".scrollin").addClass("stop");
			}
			*/
		}else{
			$this.find(".scrollin").removeClass("onscreen");
			$this.find(".scrollin").addClass("leavescreen");
			//$this.find(".scrollin").removeClass("stop");
		}
	});
	
	/*
	$(".scrollin_p").each(function(i){
	
		$(this).find(".scrollin.onscreen").not($(".scrollin.onscreen.stop")).each(function(i){
			$(this).css({
				"-webkit-transition-delay": i*150+500+"ms",
				"transition-delay": i*150+500+"ms",
			})
			
			$(this).addClass("startani");
		});
	
	});
	*/
	$(".scrollin.onscreen").not($(".scrollin_p .scrollin")).not($(".scrollin.onscreen.stop")).not($(".startani")).each(function(i){
		$(this).css({
			"-webkit-transition-delay": i*100+100+"ms",
			"transition-delay": i*100+100+"ms",
		})
		
		if($(this).hasClass("moredelay")){
			$(this).css({
				"-webkit-transition-delay": i*100+600+"ms",
				"transition-delay": i*100+600+"ms",
			})
		}
		
		if($(this).hasClass("nodelay")){
			$(this).css({
			"-webkit-transition-delay": (i-1)*100+100+"ms",
			"transition-delay": (i-1)*100+100+"ms",
			})
		}
		
		$(this).addClass("startani");
	});

	$(".scrollin_p").each(function(){
		$(this).find(".scrollin.onscreen").not($(".scrollin.onscreen.stop")).not($(".startani")).each(function(i){
			$(this).css({
				"-webkit-transition-delay": i*100+100+"ms",
				"transition-delay": i*100+100+"ms",
			})
			
			if($(this).hasClass("moredelay")){
				$(this).css({
					"-webkit-transition-delay": i*100+600+"ms",
					"transition-delay": i*100+600+"ms",
				})
			}
			
			if($(this).hasClass("nodelay")){
				$(this).css({
				"-webkit-transition-delay": (i-1)*100+100+"ms",
				"transition-delay": (i-1)*100+100+"ms",
				})
			}
			
			$(this).addClass("startani");
		});
	})
	
	
	$(".scrollin.leavescreen").each(function(i){
		$(this).css({
			"-webkit-transition-delay": 0+"ms",
    		"transition-delay": 0+"ms",
		})
		$(this).removeClass("startani");
	});
	
	$(".scrollin.stop").each(function(i){
		$(this).css({
			"-webkit-transition-delay": 0+"ms",
    		"transition-delay": 0+"ms",
		})
		$(this).addClass("startani");
	});
	
}


function loading_finish(){
	$(".loading").stop().fadeOut(function(){
		$("body").addClass("loadfinish");
		doscroll();
	});
}

function init_event(){
	/*
	$(".video_btn").click(function(){
		if($(this).hasClass("active")){
			$(this).removeClass("active")
			$(".center_video").stop().slideUp(function(){
				$(".center_video").parent().remove();
			});
		}else{
			$(this).addClass("active")
			var myhref  = $(this).attr("href");
			var $p = $(this).parents(".row");
			var $mp = $(this).parents(".col_spacing");
			var $div = $('<div class="mobile_hide"><div class="center_video col9 col"><div class="video-container"><iframe width="560" height="315" src="'+myhref+'" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe></div></div></div>');
			var $mdiv = $('<div class="mobile_show"><div class="center_video col9 col"><div class="video-container"><iframe width="560" height="315" src="'+myhref+'" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe></div></div></div>');
			$p.prepend( $div);
			$mp.prepend( $mdiv);
			$(".center_video").stop().slideDown();
		}
		return false;
	})
	*/

	$(".menu_btn").click(function(){
		if($(".menu_btn").hasClass("close")){
			$(".menu_btn").removeClass("close");
			$("body").removeClass("openmenu");
			
		}else{
			$(".menu_btn").addClass("close");
			$("body").addClass("openmenu");
		}
		return false;
	})

	$(".case_menu_btn").click(function(){
		if($(".case_menu_btn").hasClass("close")){
			$(".case_menu_btn").removeClass("close");
			$("body").removeClass("opencasemenu");
			$(".case_dropdown_menu").stop().slideUp(600);
			
		}else{
			$(".case_menu_btn").addClass("close");
			$("body").addClass("opencasemenu");
			$(".case_dropdown_menu").stop().slideDown(600);
		}
		return false;
	})
	
	$(".close_case_dropdown_menu").click(function(){
		$(".case_menu_btn").removeClass("close");
		$("body").removeClass("opencasemenu");
		$(".case_dropdown_menu").stop().slideUp(600);
		return false;
	})
}


function init_function(){
	$('.parallax-window').each(function(){
		$(this).parallax({imageSrc: $(this).attr("data-image-src")});
	});

	$(".center_slider_container").each(function(){
        var swiper = new Swiper($(this), {
            speed: 900,
            loop: true,
            slidesPerView: 1,
            spaceBetween: 0,
            autoplay: {
                delay: 6000,
                disableOnInteraction: true
            }
        });

		$(this).parents(".section").find(".center_slider_prev").click(function(){
            swiper.slidePrev();
            return false;
        })
        
		$(this).parents(".section").find(".center_slider_next").click(function(){
            swiper.slideNext();
            return false;
        })
    });
	/*
	$(".top_banner_slider_container").each(function(){
        var swiper = new Swiper($(this), {
			effect: "fade",
            speed: 900,
            loop: true,
            slidesPerView: 1,
            spaceBetween: 0,
			allowTouchMove: false,
            autoplay: {
                delay: 4500,
                disableOnInteraction: false
            }
        });
	});
	*/
}


function dosize(){
	$(".top_banner_section").height($(window).height()-$(".header_bg").height());
}

var timer; 
function dohash(){

	var hash = window.location.hash;
	
	if(hash){
		var newh;
		clearTimeout(timer);
		hash= hash.replace("#","");
		if($(".hidden_tab[data-id="+hash+"]").length){
			if(!$(".hidden_tab[data-id="+hash+"]").hasClass("active")){
				var targettop = $(".hidden_tab[data-id="+hash+"]").offset().top;
				var body = $("html, body");
				$(".tab_section").height($(".tab_section").height())
				$(".hidden_tab[data-id="+hash+"]").show();
				newh = $(".hidden_tab[data-id="+hash+"]").height();
				$(".hidden_tab[data-id="+hash+"]").hide();
				$(".hidden_tab.active").removeClass("active").hide();
				$(".hidden_tab[data-id="+hash+"]").addClass("active").stop().slideDown(600);
				$(".tab_section").height(newh)
				//body.stop().animate({scrollTop:targettop}, 1500, 'swing', function() { 
				//});
			}
			//$(".tab_btn_wrapper").addClass("hide").hide();
		}else{
			//$(".tab_btn_wrapper").removeClass("hide").stop().slideDown();
			$(".hidden_tab.active").removeClass("active").hide();
			$(".hidden_tab:first").addClass("active").show();
		}

		if($(".section[data-id="+hash+"]").length){
			var targettop = $(".section[data-id="+hash+"]").offset().top;
			var body = $("html, body");
			body.stop().animate({scrollTop:targettop-50}, 1500, 'swing', function() { 
			});
		}
		$(".menu_btn.close").click();
		timer = setTimeout(function(){
			$(".tab_section").height("auto");
		},600)
	}
	
	$(".dropdown_btn.close").removeClass("close")
}


$(function(){

	$(".media_section .col3:nth-child(4n+1)").addClass("first_col")
	$(".hidden_tab.active").show();
	init_event();
	init_function();
});

$(window).on('load', function() {
	dosize();
	doscroll();
	loading_finish();
	
	dohash();
	$(window).hashchange( function(){
		dohash();
	})
});

$(window).on('resize', function() {
	dosize();
	waitForFinalEvent(function(){
		dosize();
		doscroll();
	}, 300, "some unique string");
	
	
});



$(window).on('scroll', function() {
	doscroll();
	dosize();
});
	

