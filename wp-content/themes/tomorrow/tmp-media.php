<?php /* Template Name: Media page  */
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package tomorrow
 */

get_header();
?>


<div class="section top_title_section ">
    <div class="table_wrapper">
        <div class="table">
            <div class="td">
                <div class="top_title text0">Media</div>
            </div>
        </div>
    </div>
</div>

<div class="section full_section scrollin_p media_section">
    <div class="section_inwrapper">
        <div class="col_wrapper ">
            <div class="row">

                <!-- chung four column in a row -->
                <div class="col col3">
                    <div class="col_spacing scrollin scrollin2">
                    <div class="video-container">
                        <iframe width="560" height="315" src="https://www.youtube.com/embed/atZSgiZA3u0" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                    </div>
                    <div class="text_wrapper ">
                        Retail Media Roundtable: The Future of Retail-Based Media Platforms.  Jordan Berke interview with Rob Rivenburgh and Gal Shivtiel – November 19, 2020
                    </div>
                    </div>
                </div>

                 <div class="col col3">
                    <div class="col_spacing scrollin scrollin2">
                    <a href="https://www.scmp.com/tech/e-commerce/article/3109071/singles-day-chinese-e-commerce-giants-target-new-wave-budget" target="_blank">
                        <div class="bg_wrapper">
                            <div class="bg bg_75" style="background-image: url(<?php bloginfo('template_directory'); ?>/images/media17.jpg);"></div>
                        </div>
                        <div class="text_wrapper ">Singles’ Day: Chinese e-commerce giants target new wave of consumers – South China Morning Post interview with Jordan Berke - Nov 10, 2020
                        </div>
                    </a>
                    </div>
                </div>

                 <div class="col col3">
                    <div class="col_spacing scrollin scrollin2">
                    <a href="https://www.grocerydive.com/news/grocers-look-to-online-subscriptions-to-lock-in-digital-demand/588581/" target="_blank">
                        <div class="bg_wrapper">
                            <div class="bg bg_75" style="background-image: url(<?php bloginfo('template_directory'); ?>/images/media16.jpg);"></div>
                         <div class="text_wrapper ">Grocers look to online subscriptions to lock in digital demand – GroceryDive interview with Jordan Berke - Nov 9, 2020</div>
                        </div>
                        <div class="text_wrapper ">
                        </div>
                    </a>
                    </div>
                </div>

                <div class="col col3">
                    <div class="col_spacing scrollin scrollin2">
                    <a href="https://www.cnbc.com/2020/09/21/why-tiktok-deal-could-mean-big-growth-for-walmarts-ads-business.html" target="_blank">
                        <div class="bg_wrapper">
                            <div class="bg bg_75" style="background-image: url(<?php bloginfo('template_directory'); ?>/images/media15.jpg);"></div>
                        </div>
                        <div class="text_wrapper ">Why TikTok deal could mean big growth for Walmart’s ads business: Jordan Berke interview with CNBC - Sept 22, 2020
                        </div>
                    </a>
                    </div>
                </div>

                <div class="col col3">
                    <div class="col_spacing scrollin scrollin2">
                    <a href="https://www.cnbc.com/2020/09/21/with-tiktok-deal-walmart-could-gain-a-front-row-seat-to-the-next-generation.html" target="_blank">
                        <div class="bg_wrapper">
                            <div class="bg bg_75" style="background-image: url(<?php bloginfo('template_directory'); ?>/images/media14.jpg);"></div>
                        </div>
                        <div class="text_wrapper ">With TikTok deal, Walmart could gain ‘a front row seat to the next generation of consumers: Jordan Berke interview with CNBC - Sept 21, 2020
                        </div>
                    </a>
                    </div>
                </div>

                <div class="col col3">
                    <div class="col_spacing scrollin scrollin2">
                    <a href="https://www.grocerydive.com/news/amazon-puts-out-call-for-gig-workers-to-pick-orders-at-whole-foods/585489/" target="_blank">
                        <div class="bg_wrapper">
                            <div class="bg bg_75" style="background-image: url(<?php bloginfo('template_directory'); ?>/images/media13.jpg);"></div>
                        </div>
                        <div class="text_wrapper ">Amazon puts out call for gig workers to pick orders at Whole Foods: Interview with GroceryDive - Sept 18, 2020
                        </div>
                    </a>
                    </div>
                </div>

                <div class="col col3">
                    <div class="col_spacing scrollin scrollin2">
                    <div class="video-container">
                        <iframe width="560" height="315" src="https://www.youtube.com/embed/YdDJ4w-nKGc" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                    </div>
                    <div class="text_wrapper ">
                        Retail’s Dark Store Future.  Video posted by Tomorrow – September 2, 2020
                    </div>
                    </div>
                </div>

                <div class="col col3">
                    <div class="col_spacing scrollin scrollin2">
                    <a href="https://www.linkedin.com/pulse/grocers-beware-end-near-jordan-berke/?trackingId=PR37%2FmsvWFNbFhH6V%2FOSiQ%3D%3D" target="_blank">
                        <div class="bg_wrapper">
                            <div class="bg bg_75" style="background-image: url(<?php bloginfo('template_directory'); ?>/images/media12.jpg);"></div>
                        </div>
                        <div class="text_wrapper ">
                            Grocers Beware. The End is Near.  Article written by Jordan Berke - August 28, 2020
                        </div>
                    </a>
                    </div>
                </div>

     				<div class="col col3">
                    <div class="col_spacing scrollin scrollin2">
                    <a href="https://www.bloomberg.com/news/articles/2020-08-27/walmart-s-bid-for-tiktok-reveals-retailer-s-big-digital-ambition?sref=Lf8wk4Lq" target="_blank">
                        <div class="bg_wrapper">
                            <div class="bg bg_75" style="background-image: url(<?php bloginfo('template_directory'); ?>/images/media11.jpg);"></div>
                        </div>
                        <div class="text_wrapper ">
                            TikTok Offers Walmart a Road Map With Chinese Cousin Douyin – Bloomberg article with Jordan Berke interview - August 28, 2020
                        </div>
                    </a>
                    </div>
                </div>

                <div class="col col3">
                    <div class="col_spacing scrollin scrollin2">
                    <div class="video-container">
                        <iframe width="560" height="315" src="https://www.youtube.com/embed/h-mQVJdrZ3I" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                    </div>
                    <div class="text_wrapper ">
                        Social Commerce Part II: Jordan Berke interview with Tencent Smart Retail Director, Mark Wang – August 5, 2020
                    </div>
                    </div>
                </div>

                <div class="col col3">
                    <div class="col_spacing scrollin scrollin2">
                    <a href="https://www.grocerydive.com/news/grocers-aim-to-turn-store-safety-into-marketing-strategy/581897" target="_blank">
                        <div class="bg_wrapper">
                            <div class="bg bg_75" style="background-image: url(<?php bloginfo('template_directory'); ?>/images/media9.jpg);"></div>
                        </div>
                        <div class="text_wrapper ">
                            GroceryDive: Grocers aim to turn store safety into marketing strategy (Interview with Jordan Berke) – July 23, 2020
                        </div>
                    </a>
                    </div>
                </div>

                <div class="col col3">
                    <div class="col_spacing scrollin scrollin2">
                    <a href="https://www.bloomberg.com/news/articles/2020-07-09/walmart-s-new-challenge-to-amazon-prime-has-deep-hidden-roots?sref=Lf8wk4Lq" target="_blank">
                        <div class="bg_wrapper">
                            <div class="bg bg_75" style="background-image: url(<?php bloginfo('template_directory'); ?>/images/media10.jpg);"></div>
                        </div>
                        <div class="text_wrapper ">
                            Bloomberg: Walmart’s New Challenge to Amazon Prime Has Deep, Hidden Roots (Interview with Jordan Berke) – July 9, 2020
                        </div>
                    </a>
                    </div>
                </div>

                <div class="col col3">
                    <div class="col_spacing scrollin scrollin2">
                    <div class="video-container">
                        <iframe width="560" height="315" src="https://www.youtube.com/embed/qyhkDWSvs28" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                    </div>
                    <div class="text_wrapper ">
                        Social Commerce – Part I – A Conversation with Mark Wang, Director of Smart Retail at Tencent – June 18, 2020
                    </div>
                    </div>
                </div>

                <div class="col col3">
                    <div class="col_spacing scrollin scrollin2">
                    <a href="https://podcasts.apple.com/us/podcast/jordan-berke-digitalizing-walmart-in-china-future-retail/id1517330187?i=1000477114835" target="_blank">
                        <div class="bg_wrapper">
                            <div class="bg bg_75" style="background-image: url(<?php bloginfo('template_directory'); ?>/images/media8.jpg);"></div>
                        </div>
                        <div class="text_wrapper ">
                            Digitalizing Walmart in China and the Future of Retail – Interview on the Dao Network Podcast – June 16, 2020
                        </div>
                    </a>
                    </div>
                </div>

                <div class="col col3">
                    <div class="col_spacing scrollin scrollin2">
                    <a href="https://www.wdpartners.com/wdcast/episode-34/" target="_blank">
                        <div class="bg_wrapper">
                            <div class="bg bg_75" style="background-image: url(<?php bloginfo('template_directory'); ?>/images/media7.jpg);"></div>
                        </div>
                        <div class="text_wrapper ">
                            WDCast – Interview with Jordan Berke on Retail’s Transforming Landscape – June 14, 2020
                        </div>
                    </a>
                    </div>
                </div>

                <div class="col col3">
                    <div class="col_spacing scrollin scrollin2">
                    <a href="https://www.linkedin.com/pulse/cleanliness-new-convenience-retailers-should-embrace-themselves/" target="_blank">
                        <div class="bg_wrapper">
                            <div class="bg bg_75" style="background-image: url(<?php bloginfo('template_directory'); ?>/images/media6.jpg);"></div>
                        </div>
                        <div class="text_wrapper ">
                            Cleanliness is the new Convenience – Retailers Should Embrace the Opportunity to Differentiate Themselves, by Jordan Berke – June 11, 2020
                        </div>
                    </a>
                    </div>
                </div>

                <div class="col col3">
                    <div class="col_spacing scrollin scrollin2">
                    <div class="video-container">
                        <iframe width="560" height="315" src="https://www.youtube.com/embed/kWaMxOsKgaE" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                    </div>
                    <div class="text_wrapper ">
                        From Surge to Step Forward– How Retailers Better Serve Growing Online Demand – May 27, 2020
                    </div>
                    </div>
                </div>

                <div class="col col3">
                    <div class="col_spacing scrollin scrollin2">
                    <a href="https://www.linkedin.com/pulse/retail-has-changed-forever-have-retailers-mindsets-part-jordan-berke-1c/" target="_blank">
                        <div class="bg_wrapper">
                            <div class="bg bg_75" style="background-image: url(<?php bloginfo('template_directory'); ?>/images/media5.jpg);"></div>
                        </div>
                        <div class="text_wrapper ">
                            Retail has Changed Forever. Have Retailers Changed Their Mindsets? Part II: From E-Comm to All-Comm, by Jordan Berke – May 14, 2020
                        </div>
                    </a>
                    </div>
                </div>

                <div class="col col3">
                    <div class="col_spacing scrollin scrollin2">
                    <div class="video-container">
                        <iframe width="560" height="315" src="https://www.youtube.com/embed/A2IVNzu2xRg" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                    </div>
                    <div class="text_wrapper ">
                        Launching Mobile Checkout, Webcast by Jordan Berke, LinkedIn – May 5, 2020
                    </div>
                    </div>
                </div>

                <div class="col col3">
                    <div class="col_spacing scrollin scrollin2">
                    <a href="https://www.linkedin.com/pulse/retail-has-changed-forever-have-retailers-mindsets-part-jordan-berke/" target="_blank">
                        <div class="bg_wrapper">
                            <div class="bg bg_75" style="background-image: url(<?php bloginfo('template_directory'); ?>/images/media1.jpg);"></div>
                        </div>
                        <div class="text_wrapper ">
                            Retail Has Changed Forever.  Have Retailers Changed Their Mindsets? by Jordan Berke – April 30, 2020
                        </div>
                    </a>
                    </div>
                </div>

                <div class="col col3">
                    <div class="col_spacing scrollin scrollin2">
                    <a href="https://www.forbes.com/sites/forbestechcouncil/2020/04/20/bricks-and-clicks-the-retail-evolution-accelerates/#53d3a7e34efb" target="_blank">
                        <div class="bg_wrapper">
                            <div class="bg bg_75" style="background-image: url(<?php bloginfo('template_directory'); ?>/images/media2.jpg);"></div>
                        </div>
                        <div class="text_wrapper ">
                            Bricks and Clicks: The Retail Evolution Accelerates. Forbes article – April 20, 2020
                        </div>
                    </a>
                    </div>
                </div>

                <div class="col col3">
                    <div class="col_spacing scrollin scrollin2">
                    <div class="video-container">
                        <iframe width="560" height="315" src="https://www.youtube.com/embed/-HMZRakF9ZA?start=6" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                    </div>
                    <div class="text_wrapper ">
                        Leadership in a Crisis: Interview with leading professional Coach Vincent Gauthier about the future of ecommerce and digital innovation within retail – April 13, 2020.
                    </div>
                    </div>
                </div>

                <div class="col col3">
                    <div class="col_spacing scrollin scrollin2">
                    <a href="https://www.linkedin.com/pulse/retailers-online-channels-now-essential-millions-households-berke/" target="_blank">
                        <div class="bg_wrapper">
                            <div class="bg bg_75" style="background-image: url(<?php bloginfo('template_directory'); ?>/images/media3.jpg);"></div>
                        </div>
                        <div class="text_wrapper ">
                            Retailers’ Online Channels Now Essential to Millions of Households – March 21, 2020
                        </div>
                    </a>
                    </div>
                </div>

                <div class="col col3">
                    <div class="col_spacing scrollin scrollin2">
                    <a href="https://podcasts.apple.com/us/podcast/jordan-berke-talks-future-retail-at-2019-china-conference/id1313609715?i=1000451436931" target="_blank">
                        <div class="bg_wrapper">
                            <div class="bg bg_75" style="background-image: url(<?php bloginfo('template_directory'); ?>/images/media4.jpg);"></div>
                        </div>
                        <div class="text_wrapper ">
                            Future of Global Retail: Presentation at China Future Forum by the Hong Kong American Chamber of Commerce – November 20, 2019
                        </div>
                    </a>
                    </div>
                </div>

                <div class="clear"></div>
            </div>
        </div>
    </div>
</div>


<?php
get_footer();
